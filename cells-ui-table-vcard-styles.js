import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  @apply --cells-ui-table-vcard; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

/** DATA TABLE **/
* {
  box-sizing: border-box !important; }

html,
body {
  padding: 0;
  margin: 0; }

table {
  min-width: 100vw;
  width: auto;
  flex: 1;
  display: grid;
  border-collapse: collapse;
  /* These are just initial values which are overriden using JavaScript when a column is resized */ }

thead,
tbody,
tr {
  display: contents !important; }

th,
td {
  padding: 12px 10px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap; }

th {
  position: sticky;
  top: 0;
  background: #072146;
  text-align: left;
  font-weight: normal;
  font-size: .875rem;
  color: white;
  position: relative;
  border-right: 1px solid #fff;
  -webkit-touch-callout: none;
  /* iOS Safari */
  -webkit-user-select: none;
  /* Safari */
  -khtml-user-select: none;
  /* Konqueror HTML */
  -moz-user-select: none;
  /* Firefox */
  -ms-user-select: none;
  /* Internet Explorer/Edge */
  user-select: none;
  /* Non-prefixed version, currently
                                  supported by Chrome and Opera */ }

th:last-child {
  border: 0; }

.resize-handle {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  background: #072146;
  opacity: 0;
  width: 7px;
  cursor: col-resize; }

.resize-handle:hover,
.header--being-resized .resize-handle {
  opacity: 0.9;
  cursor: col-resize; }

td::-moz-selection {
  background-color: #388d4f;
  color: white; }

td::selection {
  background-color: #388d4f;
  color: white; }

th:hover .resize-handle {
  opacity: 0.9;
  cursor: col-resize; }

td {
  font-size: .75rem;
  color: #000;
  cursor: pointer;
  -webkit-transition: background-color 100ms linear;
  -moz-transition: background-color 100ms linear;
  -o-transition: background-color 100ms linear;
  -ms-transition: background-color 100ms linear;
  transition: background-color 100ms linear;
  border-bottom: 1px #e1e1e1 solid;
  text-transform: uppercase; }

.bg-hover,
tbody tr:hover td {
  background-color: #e3f2fd; }

tr.row-selected td {
  background-color: #49a5e6 !important;
  color: #fff; }

/*
 *  Scroll Data Table
 */
#content-data-table::-webkit-scrollbar-track {
  -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
  background-color: #f5f5f5; }

#content-data-table::-webkit-scrollbar {
  width: 5px;
  height: 12px;
  border-radius: 10px;
  background-color: #f5f5f5; }

#content-data-table::-webkit-scrollbar-thumb {
  border-radius: 10px;
  background-color: #d3d3d3; }

/** Pagination **/
.content-pagination {
  width: 100%;
  float: left;
  padding: 10px 0px; }

.display-pagination {
  width: 100%;
  text-align: center;
  position: absolute;
  height: 40px;
  padding-top: 14px;
  font-size: 0.90rem;
  color: #808080; }

@media only screen and (max-width: 480px) {
  .display-pagination {
    position: inherit;
    padding-top: 10px;
    width: 100%;
    float: left; } }

.btn-pagination {
  transition: all 0.3s ease-out;
  min-height: 2.5rem;
  min-width: 0px;
  background-color: #49a5e6;
  color: #fff; }

.btn-pagination:hover {
  background-color: #5bbeff; }

.previous {
  float: left; }

.next {
  float: right; }
`;