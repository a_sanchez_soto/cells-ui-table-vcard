import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-table-vcard-styles.js';
import '@vcard-components/cells-theme-vcard'
import '@cells-components/coronita-icons';
import '@cells-components/cells-icon';
import '@vcard-components/cells-util-behavior-vcard';
import '@bbva-web-components/bbva-button-default';
/**
This component ...

Example:

```html
<cells-ui-table-vcard></cells-ui-table-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
const cellsUtilBehaviorVcard = CellsBehaviors.cellsUtilBehaviorVcard;
export class CellsUiTableVcard extends cellsUtilBehaviorVcard(LitElement) {
  static get is() {
    return 'cells-ui-table-vcard';
  }

 // Declare properties
 static get properties() {
  return {
    currentFocus: { type: Number },
    itemsData: { type: Array },
    itemsRoot: { type: String },
    paginationRoot: { type: String },
    headersConfig: { type: Array },
    sizeColumns: { type: Number },
    itemSelected: { type: Object },
    pagination: { type: Boolean },
    paginationInMemory: { type: Boolean },
    dataInMemory: { type: Array },
    pageSize: { type: Number },
    pathNextPage: { type: String },
    currentPage: { type: Number },
    totalPage: { type: Number },
    showNextPage: { type: Boolean },
    keyEventdataTableOk: { type: Boolean }
  };
}

// Initialize properties
constructor() {
  super();
  this.currentFocus = -1;
  this.sizeColumns = 0;
  this.itemsData = [];
  this.headersConfig = [];
  this.dataInMemory = [];
  this.currentPage = 1;
  this.pageSize = this.pageSize || 10;
  this.totalPage = this.totalPage || 1;
  this.itemsRoot = this.itemsRoot || 'data';
  this.paginationRoot = this.paginationRoot || 'pagination';
  this.keyEventdataTableOk = false;
  this.paginationInMemory = true;
  this.pageSize = 10;
  this.updateComplete.then(() => {
    this.initializeConfigDataTable();
    this.initializeTable();
  });
}

/**
 * Initial component
 * @param {Object} data DataItems
 */
initializeConfigDataTable(data) {
  this.itemsData = data || this.itemsData;

  if (this.itemsData[this.itemsRoot]) {
    this.pagination = !!(this.itemsData[this.paginationRoot]);
    this.pageSize = this.pagination ? this.itemsData[this.paginationRoot].pageSize : this.pageSize;
    this.totalPage = this.pagination ? this.itemsData[this.paginationRoot].totalPages : this.totalPage;
    this.totalElements = this.pagination ? this.itemsData[this.paginationRoot].totalElements : this.itemsData[this.itemsRoot].length;
    this.pathNextPage = this.pagination ? this.getNextPage(this.itemsData[this.paginationRoot]) : '';
    this.itemsData = this.itemsData[this.itemsRoot];
  }

  if (this.paginationInMemory) {
    this.pagination = this.paginationInMemory && this.itemsData.length > 0;
    this.dataInMemory = this.itemsData;
    this.totalPage = Math.ceil(this.dataInMemory.length / this.pageSize);
    this.buildDataPaginationMemory();
  }
  this.showNextPage = this.totalPage > 1 && this.currentPage < this.totalPage;
  this.sizeColumns = this.headersConfig.length;
}

initializeTable() {
  document.addEventListener('readystatechange', () => {
    if (document.readyState === 'complete') {
      this.activeKeyUpDownSelectionDataTable();
      this.callEventsDataTable();
    }
  });
}

buildDataPaginationMemory() {
  this.itemsData = this.dataInMemory.slice((this.currentPage - 1) * this.pageSize, this.currentPage * this.pageSize);
}

nextPage() {
  if (this.paginationInMemory) {
    this._paginationMemory(+1);
  } else {
    this._paginationService(+1);
  }
}

previousPage() {
  if (this.paginationInMemory) {
    this._paginationMemory(-1);
  } else {
    this._paginationService(-1);
  }
}

/**
 * Pagination in memory
 * @param {number} i Increment Page number
 */
_paginationMemory(i) {
  this.currentPage = this.currentPage < 1 ? 1 : this.currentPage + i;
  this.showNextPage = this.totalPage > 1 && this.currentPage < this.totalPage;
  this.requestUpdate();
  this.buildDataPaginationMemory();
}

_paginationService(i) {
  this.currentPage = this.currentPage < 1 ? 1 : this.currentPage + i;
  this.showNextPage = this.totalPage > 1 && this.currentPage < this.totalPage;
  this.pathNextPage = this.pathNextPage ? this.pathNextPage.replace(/(paginationKey=).*?(&)/, `$1${this.currentPage}$2`) : this.pathNextPage;

  this.requestUpdate();
  this.dispatch('data-table-get-pagination', {
    path: this.pathNextPage,
    action: i >= 1 ? 'next' : 'previous'
  });
}

moveKeyUpDown() {
  let tr = this.shadowRoot.querySelectorAll('.row')[this.currentFocus < 0 ? 0 : this.currentFocus];
  if (!tr) {
    tr = this.shadowRoot.querySelectorAll('.row')[0];
  }
  this.selectedRow(tr);
}

selectedRow(tr) {
  this.shadowRoot.querySelectorAll('.row-selected').forEach(row => row.classList.remove('row-selected'));
  tr.classList.add('row-selected');
  let rowSelected = this.shadowRoot.querySelector('.row-selected');
  let record = (rowSelected.dataset && rowSelected.dataset.item ? rowSelected.dataset.item : null);
  if (record) {
    if (typeof record === 'string') {
      record = JSON.parse(record);
    }
    this.selectedRowData(rowSelected, record);
  }

}

selectedRowData(row, item, index) {
  this.itemSelected = item;
  if (index) {
    this.currentFocus = index;
  }
  this.requestUpdate();
  this.dispatch('selected-item-data-table', { item: item, rowSelected: row , index: index});
}

dblClick(row, item, index) {
  this.dispatch('dblClick-item-data-table', { item: item, rowSelected: row , index: index});
}

verifySelectedRowByChagePage() {
  if (this.itemSelected && this.itemSelected.id) {
    let timer = setInterval(() => {
      let rows = this.shadowRoot.querySelectorAll('.row');
      if (rows && rows.length > 0) {
        rows.forEach((row, index) => {
          if (row.dataset && row.dataset.item) {
            let item = JSON.parse(row.dataset.item);
            if (item.id === this.itemSelected.id) {
              this.selectedRow(row);
            }
          }
        });
        clearInterval(timer);
      }
    }, 100);
  }
}

activeKeyUpDownSelectionDataTable() {
  this.verifySelectedRowByChagePage();
  if (this.keyEventdataTableOk) {
    return;
  }
  window.addEventListener('keydown', (e) => {
    let rowSelected = this.shadowRoot.querySelector('.row-selected');
    if (!rowSelected) {
      this.currentFocus = -1;
    } else {
      this.shadowRoot.querySelectorAll('.row').forEach((tr, index) => {
        if (tr === rowSelected) {
          this.currentFocus = index;
        }
      });
    }
    if (e.keyCode === 40) {
      this.currentFocus++;
      this.moveKeyUpDown();
    } else if (e.keyCode === 38) {
      this.currentFocus--;
      this.moveKeyUpDown();
    } else if (e.keyCode === 39) {
      this.shadowRoot.querySelector('table').parentNode.scrollLeft = this.shadowRoot.querySelector('table').parentNode.scrollLeft + 10;
    } else if (e.keyCode === 37) {
      this.shadowRoot.querySelector('table').parentNode.scrollLeft = this.shadowRoot.querySelector('table').parentNode.scrollLeft - 10;
    } else if (e.keyCode === 13) {
      if (rowSelected) {
        let record = (rowSelected.dataset && rowSelected.dataset.item ? rowSelected.dataset.item : null);
        if (record) {
          if (typeof record === 'string') {
            record = JSON.parse(record);
          }
          this.selectedRowData(rowSelected, record);
        }
      }
    } else if (e.keyCode === 27) {
      let selectedAll = this.shadowRoot.querySelectorAll('.row-selected');
      if (selectedAll && selectedAll.length) {
        selectedAll.forEach((itemSelected) => {
          this.itemSelected = null;
          this.requestUpdate();
          itemSelected.classList.remove('row-selected');
        });
      }
    }

  });
  this.keyEventdataTableOk = true;
}

buildWidthColumns() {
  if (!this.headersConfig) {
    return '';
  }
  let widths = [];
  this.headersConfig.forEach(header => {
    widths.push(header.width ? `${header.width}` : 'auto');
  });
  return `${widths.join(' ')}`;
}

activeResizeColumnsDataTable() {
  const min = 50;
  const columnTypeToRatioMap = {
    numeric: 1,
    'text-short': 1.67,
    'text-long': 3.33,
  };

  const table = this.shadowRoot.querySelector('table');

  const columns = [];
  let headerBeingResized;
  let horizontalScrollOffset;

  const onMouseMove = (e) => requestAnimationFrame(() => {
    horizontalScrollOffset = table.parentNode.scrollLeft;
    const width = (horizontalScrollOffset + e.clientX) - headerBeingResized.offsetLeft;
    const columna = columns.find(({ header }) => header === headerBeingResized);
    columna.size = Math.max(min, width) + 'px'; // Enforce our minimum
    columns.forEach((column) => {
      if (column.size.startsWith('minmax')) { // isn't fixed yet (it would be a pixel value otherwise)
        column.size = parseInt(column.header.clientWidth, 10) + 'px';
      }
    });
    table.style.gridTemplateColumns = columns
      .map(({ header, size }) => size)
      .join(' ');
  });

  const onMouseUp = () => {
    window.removeEventListener('mousemove', onMouseMove);
    window.removeEventListener('mouseup', onMouseUp);
    headerBeingResized = null;
  };

  const initResize = ({ target }) => {
    headerBeingResized = target.parentNode;
    window.addEventListener('mousemove', onMouseMove);
    window.addEventListener('mouseup', onMouseUp);
  };

  this.shadowRoot.querySelectorAll('th').forEach((header) => {
    const max = header.dataset.type ? columnTypeToRatioMap[header.dataset.type] + 'fr' : columnTypeToRatioMap['text-short'] + 'fr';
    columns.push({
      header,
      size: `minmax(${min}px, ${max})`,
    });
    header.querySelectorAll('.resize-handle').forEach((re) => {
      re.addEventListener('mousedown', initResize);
    });
  });
}

activeRowSelectionClickDataTable() {
  this.shadowRoot.querySelectorAll('tbody tr').forEach((tr) => {
    tr.addEventListener('click', (e) => {
      this.selectedRow(tr);
    });
  });
}

callEventsDataTable() {
  this.activeResizeColumnsDataTable();
  this.activeRowSelectionClickDataTable();
}

updated(changedProperties) {
  changedProperties.forEach((oldValue, propName) => {
    if (propName === 'itemsData') {
      this.callEventsDataTable();
    }
  });
}

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-table-vcard-shared-styles').cssText}
      ${getComponentSharedStyles('cells-theme-vcard').cssText}
    `;
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <slot></slot>
    <div style="overflow-x:auto;" id="content-data-table" >
      <table style="grid-template-columns: ${this.buildWidthColumns()};">
        <thead>
          <tr>
            ${this.headersConfig.map(header => html`
              <th class="header--being-resized">${header.title} <span class="resize-handle"></span></th>
            `)}
          </tr>
        </thead>
        <tbody>

        ${this.itemsData && this.itemsData.length > 0 ?
            this.itemsData.map((item, index) => html`
            <tr data-item='${JSON.stringify(item)}' class="row" @dblclick = ${row => { this.dblClick(row, item, index) }}  @click = ${row => { this.selectedRowData(row, item, index) }}>
              ${this.headersConfig.map(header => html`
                <td>${header.columnRender ?
            this.adaptStringHtml(header.columnRender(item[header.dataField], index, item))
            : item[header.dataField]}
                </td>
              `)}
            </tr>`)
        : ''}
        </tbody>
      </table>
    </div>
    <div class = "content-pagination" ?hidden = ${(this.totalPage > this.pageSize) ? !this.pagination : true} >
      <div class = "display-pagination" >Página ${this.currentPage}/${this.totalPage} de ${this.paginationInMemory && this.dataInMemory ? this.dataInMemory.length : this.totalElements} registros.</div>
      <bbva-button-default ?hidden = ${this.currentPage === 1}  @click = ${this.previousPage} class="secondary btn-pagination previous" text="Anterior"></bbva-button-default>
      <bbva-button-default ?hidden = ${!this.showNextPage}  @click= ${this.nextPage} class="secondary btn-pagination next" text="Siguiente"></bbva-button-default>
    </div>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiTableVcard.is, CellsUiTableVcard);
