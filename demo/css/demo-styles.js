import { setDocumentCustomStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import { css, } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
      font-family: var(--cells-fontDefault, sans-serif);
      margin: 0;
  }
`);
